# MiniDebConf Curitiba 2016

Material gráfico criado para a MiniDebConf Curitiba 2016 realizada nos dias 05
e 06 de março na Aldeia Coworking.

Autor: Paulo Henrique de Lima Santana.

Arquivos SVG e PNG em vários tamanhos.

Site: http://br2016.mini.debconf.org

[![License: CC BY-SA 4.0](https://licensebuttons.net/l/by-sa/4.0/80x15.png)](https://creativecommons.org/licenses/by-sa/4.0/) 
